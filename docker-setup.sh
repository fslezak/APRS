#!/usr/bin/env bash

./wait-for-it.sh database:5432
python3 manage.py makemigrations decoder
python3 manage.py migrate
python3 manage.py runserver 0.0.0.0:8000