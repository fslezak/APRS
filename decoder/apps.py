from django.apps import AppConfig
from django.core.management import call_command
import sys

class DecoderConfig(AppConfig):
    name = 'decoder'

    def ready(self):
        if 'runserver' in sys.argv:  # If we are using runserver, not debugging or testing method
            if call_command("makemigrations", "--check") == 0:  # This returns 0 if there are migrations to apply
                call_command("migrate", interactive=False)  # Migrate
            from django.contrib.auth.models import User  # Import User database
            if not User.objects.filter(username="admin").exists(): # If superuser doesn't exist
                admin = User.objects.create_superuser('admin', 'example@12.com', '356eb703ea02be6ae6b70205b5572369b927e865b39f4bda20e498643121119b63158cdae5fc7f67275dab64a873618b386f69f26c83a5d9f3f7114f423d7a37')  # Create superuser admin
                admin.is_superuser = True
                admin.is_staff = True
                admin.save()
