from django.contrib import admin
from .models import DecodedAPRSFrames, UndecodedAPRSFrames
# Register your models here.

admin.site.register(DecodedAPRSFrames)
admin.site.register(UndecodedAPRSFrames)
